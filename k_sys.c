/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  k_sys.c
//  	-Systick interrupt and event handler implementation for a Stellaris LM3S9D92 microcontroller.
//		-Functions base on examples given by Dr. Hughes in class
//
//	Author:		Alex Doucette
//	Date:		17-10-2016
//	Project:	RTOS Assignment 2
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "k_sys.h"
#include "k_sys_int.h"
#include "k_uart0.h"
#include "p_svc.h"
#include "p_userprocess.h"
#include "mem_manager_int.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < DEFINED CONSTANTS & LOCAL VARIABLES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static volatile u32 timer_sys;

/*waiting to run queue*/
static PCB_TYPE* running_process[PRIORITY_COUNT];

/*current priority value*/
static PROC_PRIORITY current_priority;

/*pid count for unique pids*/
static int pid_cnt;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION PROTOTYPES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void k_context_switch(void);
void k_idle_process(void);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < PUBLIC FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	SEI
 * 		-Enable global interrupts
 */
void sys_sei(void){
	// enable CPU interrupts
	__asm("	cpsie	i");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	CLI
 *		-Disable global interrupts
 */
void sys_cli(void){
	// disable CPU interrupts
	__asm("	cpsid	i");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Get current primask val and disable interrupts
 */
int disable_irq(void){
	__asm(" mrs r0, PRIMASK");
	__asm(" cpsid i");
	__asm("	bx lr");
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Set interrupt mask to previous value passed through primask
 */
void enable_irq(int primask){
	if(!primask){
		__asm(" cpsie i");
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Sets device clock frequency - formerly SetupPIOSC
 */
void k_sys_set_dev_clk(void){
	/* Set BYPASS, clear USRSYSDIV and SYSDIV */
	SYSCTRL_RCC_R = (SYSCTRL_RCC_R & CLEAR_USRSYSDIV) | SET_BYPASS ;	// Sets clock to PIOSC (= 16 MHz)
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick init function
 * 		-Sets default systick default reload value
 * 		-Enables systick interrupts
 * 		-Assumes global interrupts are disabled
 *
 * 		Returns:	-TRUE on success
 * 					-FALSE on fail
 */
int k_systick_init(void){
	systick_set_period();
	systick_int_enable();
	systick_enable();

	/*set pendsv as the lowest priority interrupt*/
	NVIC_SYS_PRI3_R |= PENDSV_LOWEST_PRIORITY;

	timer_sys = TIMER_SYS_DEFAULT_VAL;

	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick init function
 * 		-Sets default systick default reload value
 * 		-Enables systick interrupts
 * 		-Assumes global interrupts are disabled
 *
 * 		Returns:	-TRUE on success
 * 					-FALSE on fail
 */
int k_process_queue_init(void){
	int i;
	for(i=0; i<PRIORITY_COUNT;i++){
		running_process[i]=NULL;
	}
	/*initial pid value*/
	pid_cnt=1;
	current_priority = IDLE;
	k_reg_proc(idle_process, pid_cnt, IDLE);
	pid_cnt++;
	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick enable function
 * 		-Sets clock source and enables the systick interrupt
 */
void systick_enable(void){
	// Set the clock source to internal and enable the counter
	NVIC_ST_CTRL_R |= NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_ENABLE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick disable function
 * 		-clears the enable bit for the systick interrupt
 * 		-halts counter operation
 */
void systick_disable(void){
	// Clear the enable bit to stop the counter
	NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_ENABLE);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Set period function
 * 		-Takes unsigned long as argument
 * 		-Sets reload value to arg[1]
 */
void systick_set_period(void){
	NVIC_ST_RELOAD_R = NVIC_ST_CTRL_COUNT;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick interrupts enable
 * 		-Enables interrupts in the NVIC controller
 */
void systick_int_enable(void){
	// Set the interrupt bit in STCTRL
	NVIC_ST_CTRL_R |= NVIC_ST_CTRL_INTEN;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Systick interrupt disable
 * 		-Clears the interrupt for systick in the nvic controller
 */
void systick_int_disable(void){
	// Clear the interrupt bit in STCTRL
	NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_INTEN);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	SYSTICK interrupt service routine implementation
 *		-Made using functions from sample SVC handler supplied by DR. HUGHES
 *		-Should not need to disable interrupts in this isr
 */
void SYSTICK_ISR(void){
	/*Here we set pendsv to interrupt and go back to what we were doing*/
	NVIC_INT_CTRL_R |= TRIGGER_PENDSV;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Register Process function
 * 		-Context switch function
 * 		-Provides code common to both the Systick implementation and pendsv
 */
void k_context_switch(void){
	running_process[current_priority]->sp_current = get_PSP();
	running_process[current_priority]=running_process[current_priority]->next_pcb;
	set_PSP(running_process[current_priority]->sp_current);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	PENDSV interrupt service routine implementation
 *		-Disable interrupts during the SYSTICK
 */
void PENDSV_ISR(void){
	int primask = disable_irq();
	/*only ever called from psp (lowest priority)*/
	save_registers();
	k_context_switch();
	restore_registers();
	enable_irq(primask);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Register Process function
 * 		-Function allocates a PCB and STACK for the process
 * 		-Additionally assigns it to the proper priority queue
 * 		-This is a KERNEL function - should not need to disable interrupts
 * 			as preemption by systick will result in a pendsv.
 *
 *		-Note that there is no limit on the number of entries in each list of processes,
 *		we rely on memory limitations to limit number of processes using default stack size.
 *
 * 		Returns: 	FALSE on fail
 * 					TRUE on success
 */

int k_reg_proc(void (*func)(void), unsigned pid, PROC_PRIORITY priority)
{
	/*disable interruts*/
	int primask = disable_irq();

	STACK_FRAME_TYPE* new_sf;
	/*allocate new PCB and check if valid*/
	PCB_TYPE* new_pcb = (PCB_TYPE*)alloc(sizeof(PCB_TYPE));
	if(new_pcb==NULL){
		enable_irq(primask);
		return FALSE;
	}
	/*allocate new stack pointer and check if valid*/
	new_pcb->sp_initial = (u32)alloc(STACKSIZE);
	if(new_pcb->sp_initial==NULL){
		/*attempt to dealloc the pcb if we return due to sp allocation error*/
		dealloc((u32*)new_pcb);
		enable_irq(primask);
		return FALSE;
	}
	new_pcb->sp_current = (new_pcb->sp_initial + STACKSIZE) - sizeof(STACK_FRAME_TYPE);
	new_pcb->pid = pid;
	/*sp_current now points to the top of the stack frame*/
	new_sf = (STACK_FRAME_TYPE*) new_pcb->sp_current;

	/*Fill the stack with initial values*/
	new_sf->xpsr = (u32)INIT_PSR;
	new_sf->lr = (u32)p_terminate;
	new_sf->pc = (u32)func;

	k_add_new_process(new_pcb, priority);

	/*restore interrupts*/
	enable_irq(primask);
	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Taken from Dr. Hughes Example
 * 	-Doesn't seem super necessary to divide SVC_ISR and SVCHandler into two separate
 * 	functions other than organizational "niceness"
 * 	-This way we have all the assembly in one and then we pass stuff as "arguments" so
 * 	they're easily accessed in c in the handler.
 *
 * Supervisor call (trap) entry point
 * Using MSP - trapping process either MSP or PSP (specified in LR)
 * Source is specified in LR: F9 (MSP) or FD (PSP)
 * Save r4-r11 on trapping process stack (MSP or PSP)
 * Restore r4-r11 from trapping process stack to CPU
 * SVCHandler is called with r0 equal to MSP or PSP to access any arguments
 */

void SVC_ISR(void){
	/* Save LR for return via MSP or PSP */
	__asm(" PUSH 	{LR}");

	/* Trapping source: MSP or PSP? */
	__asm(" TST 	LR,#4");	/* Bit 4: MSP (0) or PSP (1) */
	__asm(" BNE 	RtnViaPSP");	/* EQ zero - MSP; NE zero - PSP */

	/* Trapping source is MSP - save r4-r11 on stack (default, so just push) */
	__asm(" PUSH 	{r4-r11}");
	__asm(" MRS	r0,msp");	/*passing MSP as argument to SVCHandler*/
	__asm(" BL	SVCHandler");	/* r0 is MSP */
	__asm(" POP	{r4-r11}");
	/*arbitrary pop due to compiler pushing R3, LR on entry*/
	__asm(" POP {R0}");
	__asm(" POP {R0}");
	__asm(" POP {PC}");

	/* Trapping source is PSP - save r4-r11 on psp stack (MSP is active stack) */
	__asm("RtnViaPSP:");
	/* Save r4..r11 on process stack */
	save_registers();
	__asm(" BL	SVCHandler");	/* r0 Is PSP */

	/* Restore r4..r11 from trapping process stack  */
	restore_registers();
	/*arbitrary pop due to compiler pushing R3, LR on entry*/
	__asm(" POP {R0}");
	__asm(" POP {R0}");
	__asm(" POP {PC}");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Taken from the work of Dr. Hughes
 *
 * Supervisor call handler
 * Handle startup of initial process
 * Handle all other SVCs such as getid, terminate, etc.
 * Assumes first call is from startup code
 * Argptr points to (i.e., has the value of) either:
   - the top of the MSP stack (startup initial process)
   - the top of the PSP stack (all subsequent calls)
 * Argptr points to the full stack consisting of both hardware and software
   register pushes (i.e., R0..xPSR and R4..R10); this is defined in type
   stack_frame
 * Argptr is actually R0 -- setup in SVCall(), above.
 * Since this has been called as a trap (Cortex exception), the code is in
   Handler mode and uses the MSP
 */
void SVCHandler(STACK_FRAME_TYPE *argptr){
	PCB_TYPE* temp;
	PROC_PRIORITY temp_priority;
	KCALL_ARGS_TYPE *kcaptr;
	kcaptr = (KCALL_ARGS_TYPE*) argptr -> r7;
	kcaptr->rtnvalue = FALSE;
	switch(kcaptr->code){
		case STARTUP:
			running_process[current_priority]->sp_current += NUM_REG_PUSHED * sizeof(unsigned int);
			set_PSP(running_process[current_priority]->sp_current);
			k_systick_init();
			/*
			 - Change the current LR to indicate return to Thread mode using the PSP
			 - Assembler required to change LR to FFFF.FFFD (Thread/PSP)
			 - BX LR loads PC from PSP stack (also, R0 through xPSR) - "hard pull"
			*/
			__asm("	movw 	LR,#0xFFFD");  /* Lower 16 [and clear top 16] */
			__asm("	movt 	LR,#0xFFFF");  /* Upper 16 only */
			__asm("	bx 	LR");          /* Force return to PSP */
		break;

		case GETID:
			kcaptr->rtnvalue = running_process[current_priority]->pid;
		return;


		case NICE:
			if((kcaptr->arg1-current_priority) == 0){
				kcaptr->rtnvalue = TRUE;
				return;
			}
			else{
				/*in case of error just return false*/
				temp_priority = (PROC_PRIORITY)(kcaptr->arg1);
				if(temp_priority>=IDLE){
					kcaptr->rtnvalue=FALSE;
					return;
				}
				/*requested priority < or > current priority*/
				temp = running_process[current_priority];
				/*store current sp in case of context switch*/
				temp->sp_current = get_PSP();
				k_remove_current_process();
				k_add_new_process(temp, temp_priority);

				/*our running_process now points to new process to run, finish a context switch*/
				set_PSP(running_process[current_priority]->sp_current);
				kcaptr->rtnvalue = TRUE;
			}
		break;

		case GETPRIORITY:
			kcaptr->rtnvalue = current_priority;
		return;

		case UARTSEND:
			kcaptr->rtnvalue = k_uart0_send_string((char*)(kcaptr->arg1));
		return;

		case TERMINATE:
			/*deallocate stack pointer*/
			dealloc((u32*)running_process[current_priority]->sp_initial);
			/*check if there are other entries in the list */
			temp = running_process[current_priority];
			k_remove_current_process();
			dealloc((u32*)temp);
			/*get the next process ready to run - we pop the stuff back in the svc*/
			set_PSP(running_process[current_priority]->sp_current);
		break;

		default:
		break;
	}
	return;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	remove_current_process
 * 		-Removes current process from queue
 * 		-Used by terminate and nice functions
 */
void k_remove_current_process(void){
	if(running_process[current_priority]->next_pcb == running_process[current_priority]){
		/*here we have one entry in list*/
		running_process[current_priority] = NULL;
		while(running_process[current_priority]==NULL&&current_priority<=IDLE){
			/*increment to next priority with available processes*/
			current_priority++;
		}
	}else{
		/*multiple entries in list, so join neighbors*/
		running_process[current_priority]->prev_pcb->next_pcb=running_process[current_priority]->next_pcb;
		running_process[current_priority]->next_pcb->prev_pcb=running_process[current_priority]->prev_pcb;
		running_process[current_priority]=running_process[current_priority]->next_pcb;
	}
	return;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*Add the new process into the running queue
	 * 3 cases: no entries in list				ptr==null
	 * 			one or many entries in list. 	otherwise
	 *
	 *	NOTE: 	last two cases (1 or many) handled by referencing the ptr->prev->next = newpcb
	 *			followed by ptr->prev=newpcb to finish linking the list.
	 *			This fixes both of the broken links when we added the new pcb in.
	 *
	 */
void k_add_new_process(PCB_TYPE* new_pcb, PROC_PRIORITY priority){
	/*add process*/
	if(running_process[priority] == NULL){
		/*no entries in list*/
		new_pcb->prev_pcb = new_pcb;
		new_pcb->next_pcb = new_pcb;
		running_process[priority]=new_pcb;
	}
	else{
		new_pcb->prev_pcb = running_process[priority]->prev_pcb;
		new_pcb->next_pcb = running_process[priority];
		running_process[priority]->prev_pcb->next_pcb = new_pcb;
		running_process[priority]->prev_pcb = new_pcb;
	}

	/*lastly check if this process is of higher priority*/
	if(priority<current_priority){
		/*TODO:: 	This might cause problems if we call this inside a process
		*		 	The context switch will be wonky and won't go to next of the "current" priority
		*			WIll have to add some "special code" to fix this
		*/
		current_priority = priority;
	}
}


