/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  k_uar0.c
//  	-Uart0 implementation for stellaris kernel
//
//	Author:		Alex Doucette
//	Date:		17-10-2016
//	Project:	RTOS Assignment 2
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "k_uart0.h"
#include "k_sys_int.h"
#include "utils_cqueue.h"
#include "mem_manager_int.h"
#include "k_reg_access.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < DEFINED CONSTANTS & LOCAL VARIABLES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static volatile u8 uart0_busy;
static volatile u8 outstring_index;
static QUEUE_TYPE* output_queue;
static volatile char* output_string;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION PROTOTYPES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < PUBLIC FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	uart0 initialization function
 *
 * 		Returns: 	True on success
 * 					False on error
 */
int k_uart0_init(void){
	/* Initialise UART0 */
	SYSCTL_RCGC1_R |= SYSCTL_RCGC1_UART0; 				// Enable Clock Gating for UART0
	SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOA; 				// Enable Clock Gating for PORTA

	UART0_CTL_R &= ~UART_CTL_UARTEN;      				// Disable the UART

	// Setup the BAUD rate
	UART0_IBRD_R = 8;									// IBRD = int(16,000,000 / (16 * 115,200)) = int(8.68055)
	UART0_FBRD_R = 44;									// FBRD = int(0.68055 * 64 + 0.5) = 44.0552

	UART0_LCRH_R = (UART_LCRH_WLEN_8);					// WLEN: 8, no parity, one stop bit, without FIFOs)
	UART0_CTL_R = UART_CTL_UARTEN;        				// Enable the UART and End of Transmission Interrupts

	GPIO_PORTA_AFSEL_R |= EN_RX_PA0 | EN_TX_PA1;    	// Enable Receive and Transmit on PA1-0
	GPIO_PORTA_DEN_R |= EN_DIG_PA0 | EN_DIG_PA1;   		// Enable Digital I/O on PA1-0

	/* enable interrupts */
	NVIC_EN0_R |= (1 << INT_UART0);						// Enable the interrupt in the EN0 Register - Indicate to CPU which device is to interrupt
	UART0_IM_R |= (UART_INT_RX | UART_INT_TX);			// Set specified bits for interrupt

	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	output queue and handler init function
 *
 * 		Returns: 	True on success
 * 					False on error
 */
int k_uart0_outqueue_init(void){
	/*set output busy to false*/
	uart0_busy=FALSE;

	output_queue = (QUEUE_TYPE*) alloc(sizeof(QUEUE_TYPE));

	/* Initialize the queue with assigned head/tail values */
	cqueue_init(output_queue);

	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 	Begins transmission and adds to the out queue
 * 		-If transmission in progress just adds to the output queue.
 */
int k_uart0_send_string(char* data){
	int primask=disable_irq();
	/*always add to the queue initially*/
	if(!cqueue_enq(output_queue, data)){
		enable_irq(primask);
		return FALSE;
	}
	if(!uart0_busy){
		/*uart is not busy - set to busy*/
		uart0_busy=TRUE;

		output_string=(volatile char*)cqueue_deq(output_queue);
		/*set initial outstring index value*/
		outstring_index = 0;
		UART0_DR_R = output_string[outstring_index];
		outstring_index++;
	}
	enable_irq(primask);
	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *	UART0 isr implementation
 *
 */
void UART0_ISR(void){
	volatile char ch;
	if (UART0_MIS_R & UART_INT_RX){
		/*PLACEHOLDER: takes input characters and discards them to clear input interrupt*/
		/*Worry about this for the next assignment - A3 requires no user input*/

		/* RECV done - clear interrupt and make char available to application */
		UART0_ICR_R |= UART_INT_RX;
		ch = UART0_DR_R;
	}
	if(UART0_MIS_R & UART_INT_TX){
		/* char XMIT done - clear interrupt */
		UART0_ICR_R |= UART_INT_TX;
		if(output_string[outstring_index] != '\0' && outstring_index<=MAX_STRING_INDEX){
			/*set dr to next available char in the output string at the head*/
			UART0_DR_R = output_string[outstring_index];
			outstring_index++;
		}
		else{
			/*reset the index*/
			outstring_index = 0;
			/*dequeue the existing empty head*/
			output_string = (volatile char*)cqueue_deq(output_queue);
			if(output_string != NULL ){
				UART0_DR_R = output_string[outstring_index];
				outstring_index++;
			}
			else{
				uart0_busy=FALSE;
			}
		}
	}
}
