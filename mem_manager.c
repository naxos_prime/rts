/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  mem_manager.c
//  	-Memory manager implementation for a Stellaris LM3S9D92 microcontroller.
//
//	Author:		Alex Doucette
//	Date:		26-09-2013
//	Project:	RTOS Assignment 1
//	TODO: 		Bring the mem_alloc and mem_de_alloc logic into their respective calling functions to reduce the number of function calls made.
//					->maybe compiler already does it through optimisation?
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "mem_manager.h"
#include "mem_manager_int.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < DEFINED CONSTANTS & LOCAL VARIABLES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#define sim_testing
#define ELEMENTS_PER_LINE 	8
#define E_OFFSET(x) 		(x)*ELEMENTS_PER_LINE

/* Local arrays used to keep track of the blocks in use in each memory segment */
static u16 arr_2KiB[MAX_2KiB_BLOCKS + A_OFFSET];
static u16 arr_1KiB[MAX_1KiB_BLOCKS + A_OFFSET];
static u16 arr_512B[MAX_512B_BLOCKS + A_OFFSET];
static u16 arr_256B[MAX_256B_BLOCKS + A_OFFSET];
static u16 arr_128B[MAX_128B_BLOCKS + A_OFFSET];

/* Local structs used for memory organisation named as per the segment block size */
static MEM_BLOCK mem_block_2KiB;
static MEM_BLOCK mem_block_1KiB;
static MEM_BLOCK mem_block_512B;
static MEM_BLOCK mem_block_256B;
static MEM_BLOCK mem_block_128B;

static MEM_BLOCK* mem_segments[NUM_SEGMENTS] = {&mem_block_2KiB, &mem_block_1KiB, &mem_block_512B, &mem_block_256B, &mem_block_128B};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION PROTOTYPES >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static u32 mem_alloc(u8 index);
static u8 mem_dealloc(u32 requested_addr, u8 index);
static void mem_block_init(MEM_BLOCK *p_block, u16 block_size, u16 num_blocks, u16* array, u32 s_address);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < PUBLIC FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Initialisation function for the memory manager
 * 	-Takes no arguments
 * 	-Sets initial values
 *	-TODO: reduce "repeated" code when assigning values to the structs
 *
 *	-Returns:	-TRUE on success
 *				-No error returns currently implemented
 *
 */
int mem_manager_init(void) {
	/* see extras.c for alternative implementation of HEAP_START_ADDR calculation ensuring space requirements and word alignment */
	mem_block_init(&mem_block_2KiB, BS_2KiB, MAX_2KiB_BLOCKS, arr_2KiB, HEAP_START_ADDR);

	/* chain the segments together - note that the end address+1 is used for the next block start address in the case of "HEAP_START_ADDR" is ever variable */
	mem_block_init(&mem_block_1KiB, BS_1KiB, MAX_1KiB_BLOCKS, arr_1KiB, (mem_block_2KiB.end_address+A_OFFSET));
	mem_block_init(&mem_block_512B, BS_512B, MAX_512B_BLOCKS, arr_512B, (mem_block_1KiB.end_address+A_OFFSET));
	mem_block_init(&mem_block_256B, BS_256B, MAX_256B_BLOCKS, arr_256B, (mem_block_512B.end_address+A_OFFSET));
	mem_block_init(&mem_block_128B, BS_128B, MAX_128B_BLOCKS, arr_128B, (mem_block_256B.end_address+A_OFFSET));

	return TRUE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Debug function
 * 	-Prints debug information to the console while running on the simulator
 * 	-Includes start/end index pointers for each segment and the queue arrays
 *
 * 	-Returns:	-Undefined
 *
 */
void mem_manager_debug_print(void){
#ifdef sim_testing
	int i;
	int j;

	for(i=0;i<NUM_SEGMENTS;i++){
		printf("\n//--------------------------------------------------------------\n");
		printf("// Block Size:%16c0x%04x\n", ' ', mem_segments[i]->b_size);
		printf("// Number of Blocks:%10c%hu\n", ' ', mem_segments[i]->b_number);
		printf("// Segment Base Address:%6c0x%8x\n", ' ', mem_segments[i]->start_address);
		printf("// Queue Front Pointer:%7c%hu\n", ' ', mem_segments[i]->s_index);
		printf("// Queue End Pointer:%9c%hu\n", ' ', mem_segments[i]->e_index);
		printf("// INDEX value: ROW + COLUMN\n");
		printf("//--------------------------------------------------------------\n");
		/* Print 8 entries per line */
		/* Assumes we have num blocks divisible by 8 */
		printf("%6c0%6c1%6c2%6c3%6c4%6c5%6c6%6c7%6c\n", ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
		for(j=0;j<(mem_segments[i]->b_number / ELEMENTS_PER_LINE);j++){
			/* this is so ugly */
			printf("%05hu %05hu  %05hu  %05hu  %05hu  %05hu  %05hu  %05hu  %05hu\n", E_OFFSET(j), mem_segments[i]->b_array[E_OFFSET(j)], mem_segments[i]->b_array[E_OFFSET(j)+1], mem_segments[i]->b_array[E_OFFSET(j)+2], mem_segments[i]->b_array[E_OFFSET(j)+3], mem_segments[i]->b_array[E_OFFSET(j)+4], mem_segments[i]->b_array[E_OFFSET(j)+5], mem_segments[i]->b_array[E_OFFSET(j)+6], mem_segments[i]->b_array[E_OFFSET(j)+7]);
		}
		/* print the last entry we started at 0, num_blocks+1 total elements */
		printf("%05hu %05hu\n", E_OFFSET(j), mem_segments[i]->b_array[E_OFFSET(j)]);
	}
#endif
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Allocation function for the memory manager
 * 	-Takes a 32 bit size as the input
 * 	-Allocation uses a linear search through block sizes to find the appropriate segment to use
 * 	-This should present minimal limitations when compared to a linear search if we assume that while the number of blocks might
 * 	 increase the number of block sizes will remain relatively low in a scaling system
 *
 * 	-Returns:	-32bit address on successful allocation
 * 				-NULL on an error
 *
 */
u32* alloc(u16 requested_size){
	int i = 0;
	i=(NUM_SEGMENTS - A_OFFSET);
	while(i>=0){
		if( (requested_size <= mem_segments[i]->b_size) && (mem_segments[i]->s_index != 0)){
			/* found a memory segment with free blocks (index !=0) and whose blocks size is greater than the requested size */
			return (u32*)mem_alloc(i);
		}
		else{
			i--;
		}
	}

#ifdef sim_testing
	printf("ALLOCATION ERROR: No free blocks available to accommodate requested size.\n");
	printf("ERROR ARGUMENT:   %04x\n", requested_size);
#endif
	return NULL;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Deallocation function for the memory manager
 *	-Again here assume address range is related directly to the block sizes of which we have a relatively small amount
 *	-Linear search to find which address range our address fits within
 *
 *	Returns: 	-Success:	TRUE
 *				-ERROR: 	FALSE
 *
 */
u8 dealloc(u32* addr){
	int i = 0;
	u32 requested_addr;
	requested_addr = (u32)addr;
	if((requested_addr & WORD_ALIGN_MSK) == 0){
		/* requested address was word aligned ! */
		i=(NUM_SEGMENTS - A_OFFSET);
		while(i>=0){
			if( (requested_addr <= mem_segments[i]->end_address) && (requested_addr >= mem_segments[i]->start_address)){
				/* we found which segment of the heap the address belongs in */
				return mem_dealloc(requested_addr, i);
			}
			else{
				i--;
			}
		}
	}

#ifdef sim_testing
	printf("DEALLOCATION ERROR: Requested address not word aligned or outside range.\n");
	printf("ERROR ARGUMENT:     %08x\n", requested_addr);
#endif
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                     < LOCAL FUNCTION IMPLEMENTATION >
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Memory de-allocation function
 * 	-Takes a valid (32bit word aligned and within range) address and valid index value as arguments
 * 	-Attempts to "deallocate" the address it from the heap by returning the memory block into the queue
 *
 * 	Returns: 	-Success: 	TRUE
 * 				-ERROR: 	FALSE
 */
static u8 mem_dealloc(u32 requested_addr, u8 index){
	u32 temp_var;
	/* Check that the requested address is in fact a valid block pointer*/
	temp_var = requested_addr - mem_segments[index]->start_address;
	if((temp_var % mem_segments[index]->b_size) == 0){
		/* so now yes we have a valid block address */
		temp_var = ((temp_var / mem_segments[index]->b_size) + A_OFFSET);

		/* temp_var is now the index of the block in the allocation array */
		if((mem_segments[index]->b_array[temp_var]==0) && (mem_segments[index]->e_index != temp_var)){
			/* the address was in use, now deallocate it by adding it into the unused queue*/
			/* we don't need to reset it's value to 0 since it already is */
			if(mem_segments[index]->e_index!=0){
				/* if our end index was not 0 we add it to the queue */
				mem_segments[index]->b_array[mem_segments[index]->e_index] = temp_var;
			}

			if(mem_segments[index]->s_index == 0){
				/* if our queue was empty set the head equal to the tail */
				mem_segments[index]->s_index = temp_var;
			}
			mem_segments[index]->e_index = temp_var;
			return TRUE;
		}
		else{

#ifdef sim_testing
			printf("DEALLOCATION ERROR: Block not allocated.\n");
			printf("ERROR ARGUMENT:     %08x\n", requested_addr);
			printf("MEM_BLOCK INDEX:    %hu\n", index);
#endif

			return FALSE;
		}
	}

#ifdef sim_testing
	printf("DEALLOCATION ERROR: Address not valid block pointer.\n");
	printf("ERROR ARGUMENT:     %08x\n", requested_addr);
	printf("MEM_BLOCK INDEX:    %hu\n", index);
#endif

	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Returns the address from the top of the queue of the selected indexed segment
 *	-Cannot be called unless we already know that a value exists in the queue (see: function call in u32 alloc(u16 requested_size))
 *
 */
static u32 mem_alloc(u8 index){
	u16 temp;

	/* store the top of the queue index value in temp*/
	temp = mem_segments[index]->s_index;

	/* increment the queue start pointer to the next value */
	mem_segments[index]->s_index = mem_segments[index]->b_array[temp];

	if(mem_segments[index]->s_index == 0){
		/* if we were just pointing at the same entry and there is now 0 left in the queue set the end pointer to 0 */
		mem_segments[index]->e_index =0;
	}

	/* remove the top of the queue from the list */
	mem_segments[index]->b_array[temp]=0;

	/*calculate the block address from the start address and an offset of the block size * (index - 1) */
	return (mem_segments[index]->start_address + (mem_segments[index]->b_size * (temp - A_OFFSET)) );
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * "Constructor" for the MEM_BLOCK struct
 *  -Assigns values based on the information passed
 * 	-No default arguments currently defined
 * 	-Assumes MEM_BLOCK has already been allocated
 *
 * 	Returns: undefined.
 */
static void mem_block_init(MEM_BLOCK *p_block, u16 block_size, u16 num_blocks, u16* array, u32 s_address){
	u16 i = 0;
	p_block->start_address = s_address;
	//end address is at "n-1", since we begin at 0
	p_block->end_address = s_address + (num_blocks * block_size) - A_OFFSET;

	/* setup initial queue */
	p_block->b_array = array;
	p_block->s_index = 1;
	for(i=1;i<num_blocks;i++){
		p_block->b_array[i] = i+1;
	}
	/* set the last entry in the queue to point to 0 "next" */
	p_block->b_array[num_blocks] = 0;

	p_block->e_index = num_blocks;
	p_block->b_size = block_size;
	p_block->b_number = num_blocks;
}
