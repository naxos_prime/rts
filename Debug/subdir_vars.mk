################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lm3s9d92.cmd 

C_SRCS += \
../k_reg_access.c \
../k_sys.c \
../k_uart0.c \
../lm3s9d92_startup_ccs.c \
../main.c \
../mem_manager.c \
../p_svc.c \
../p_userprocess.c \
../utils_cqueue.c 

OBJS += \
./k_reg_access.obj \
./k_sys.obj \
./k_uart0.obj \
./lm3s9d92_startup_ccs.obj \
./main.obj \
./mem_manager.obj \
./p_svc.obj \
./p_userprocess.obj \
./utils_cqueue.obj 

C_DEPS += \
./k_reg_access.pp \
./k_sys.pp \
./k_uart0.pp \
./lm3s9d92_startup_ccs.pp \
./main.pp \
./mem_manager.pp \
./p_svc.pp \
./p_userprocess.pp \
./utils_cqueue.pp 

C_DEPS__QUOTED += \
"k_reg_access.pp" \
"k_sys.pp" \
"k_uart0.pp" \
"lm3s9d92_startup_ccs.pp" \
"main.pp" \
"mem_manager.pp" \
"p_svc.pp" \
"p_userprocess.pp" \
"utils_cqueue.pp" 

OBJS__QUOTED += \
"k_reg_access.obj" \
"k_sys.obj" \
"k_uart0.obj" \
"lm3s9d92_startup_ccs.obj" \
"main.obj" \
"mem_manager.obj" \
"p_svc.obj" \
"p_userprocess.obj" \
"utils_cqueue.obj" 

C_SRCS__QUOTED += \
"../k_reg_access.c" \
"../k_sys.c" \
"../k_uart0.c" \
"../lm3s9d92_startup_ccs.c" \
"../main.c" \
"../mem_manager.c" \
"../p_svc.c" \
"../p_userprocess.c" \
"../utils_cqueue.c" 


